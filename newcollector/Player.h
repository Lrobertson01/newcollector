#pragma once

#include <sfml\Graphics.hpp>

#include <vector>

class Player
{
	public:

	//constructor
	Player(sf::Texture& playertexture, sf::Vector2u screensize);

	//variables
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	int playerIndex; //sets whether the sprite in question is player 1 or player 2
	int score = 0;

	void Input();
	void Update(sf::Time frameTime);
	void reset(sf::Vector2u screensize);
	sf::Text scoreText; //move scoretext into the class so we dont need to intialise it differently for each player
	sf::Text gameOverText; //moved into here for the same reason as scoretext, there would be 2 gameover texts otherwise
};

