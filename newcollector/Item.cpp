#include "Item.h"
#include <cstdlib>

//constructor
Item::Item(std::vector<sf::Texture>& itemTextures, sf::Vector2u screensize)
{
	//sets the index to be randomised
	int chosenindex = rand() % itemTextures.size();
	sprite.setTexture(itemTextures[chosenindex]);
	pointsvalue = chosenindex * 100 + 100;

	int positionX = rand() % (screensize.x - itemTextures[chosenindex].getSize().x);
	int positionY = rand() % (screensize.y - itemTextures[chosenindex].getSize().y);

	sprite.setPosition(positionX, positionY);
	
}
