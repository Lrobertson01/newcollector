#pragma once
#include <SFML/Graphics.hpp>;
#include <vector>

class Item
{
public:	

	//constructor
	Item(std::vector < sf::Texture>& itemTextures, sf::Vector2u screensize);

	sf::Sprite sprite;
	int pointsvalue;
	sf::Time itemTime = sf::seconds(10.0f);
};

