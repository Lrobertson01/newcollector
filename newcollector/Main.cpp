#include <string>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include "Item.h"
#include <cstdlib>
#include <time.h>
#include "Player.h"

// The main() Function - entry point for our program
int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window
	gameWindow.create(sf::VideoMode::getDesktopMode(), "The Collector", sf::Style::Titlebar | sf::Style::Close);
	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	//Set variable to be Random via connecting to time
	srand(time(NULL));
	//set gameover variable
	bool gameover = false;

	// Player Sprite
	// Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");

	Player playerObject(playerTexture, gameWindow.getSize());
	Player playerObject2(playerTexture, gameWindow.getSize());

	playerObject.playerIndex = 0;
	playerObject2.playerIndex = 1;


	// Game Music
	// Declare a music variable called gameMusic
	sf::Music gameMusic;
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.play();

	//sound effect/s
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/Pickup.wav");

	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");

	sf::Sound pickUpSound;
	pickUpSound.setBuffer(pickupSoundBuffer);

	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);

	// Game Font
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");

	// Title Text
	sf::Text titleText;
	titleText.setFont(gameFont);
	titleText.setString("The Collector");
	titleText.setCharacterSize(24);
	titleText.setFillColor(sf::Color::Cyan);
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);


	// Author Text
	sf::Text authorText;
	authorText.setFont(gameFont);
	authorText.setString("by Lewis Robertson");
	authorText.setCharacterSize(16);
	authorText.setFillColor(sf::Color::Magenta);
	authorText.setStyle(sf::Text::Italic);
	authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);

	// Score
	//Because the class doesnt contain a definition for Font, and reinitialising it in there would just be redundant, i set the font here despite most of the info being inside the class
	playerObject.scoreText.setFont(gameFont);
	playerObject2.scoreText.setFont(gameFont);


	// Gameover Text

	//Set here for same reason as scoreTexts font
	playerObject.gameOverText.setFont(gameFont);
	playerObject2.gameOverText.setFont(gameFont);

	//Set here, as the only alternative is setting it inside one of the update functions of the class itself, which runs the line every single frame which is inefficient and bad practice
	playerObject.gameOverText.setString("Player 1 Wins");
	playerObject2.gameOverText.setString("Player 2 Wins");

	//Set here as the class doesnt have a definition of game window, and if i try to use screensize it ends up slightly off centre which is distracting
	playerObject.gameOverText.setPosition(gameWindow.getSize().x / 2 - playerObject.gameOverText.getLocalBounds().width / 2, 150);
	playerObject2.gameOverText.setPosition(gameWindow.getSize().x / 2 - playerObject2.gameOverText.getLocalBounds().width / 2, 150);
	

	// Gameover Text
	sf::Text restartText;
	restartText.setFont(gameFont);
	restartText.setString("Press R to restart");
	restartText.setCharacterSize(24);
	restartText.setFillColor(sf::Color::Cyan);
	restartText.setStyle(sf::Text::Bold | sf::Text::Italic);
	restartText.setPosition(gameWindow.getSize().x / 2 - restartText.getLocalBounds().width / 2, 250);

	// Timer
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	sf::Time timeLimit = sf::seconds(60.0f);
	sf::Time timeRemaining = timeLimit;
	sf::Clock gameClock;


	//Item Spawning
	sf::Time itemspawnduration = sf::seconds(2.0f);
	//create a timer for the time remaining in the game
	sf::Time itemspawnremaining = itemspawnduration;

	// Items

	std::vector<Item> items;

	std::vector<sf::Texture> itemTextures;
	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());
	itemTextures[0].loadFromFile("Assets/Graphics/coinBronze.png");
	itemTextures[1].loadFromFile("Assets/Graphics/coinSilver.png");
	itemTextures[2].loadFromFile("Assets/Graphics/coinGold.png");

	// Load up a few random starting items - 1 of each type)
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));



	// -----------------------------------------------
	// End of Game Setup
	// -----------------------------------------------


	// Game Loop
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		sf::Event gameEvent;
		while (gameWindow.pollEvent(gameEvent))
		{
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		}
		//movement
		playerObject.Input();
		playerObject2.Input();



		// End event polling loop
		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		sf::Time frameTime = gameClock.restart();
		timeRemaining = timeRemaining - frameTime;
		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));
		playerObject.scoreText.setString("P1 Score: " + std::to_string(playerObject.score));
		playerObject2.scoreText.setString("P2 Score: " + std::to_string(playerObject2.score));
		playerObject.Update(frameTime); //move the player
		playerObject2.Update(frameTime);

		//sets the timer to 0 so as to test easier
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) 
		{
			timeRemaining = timeRemaining - timeRemaining; //it would not let me set it directly to the int 0, so this acomplishes the same thing.
		}

		//Item despawning after a specific time frame
		for (int i = items.size() - 1; i >= 0; --i)
		{
			items[i].itemTime = items[i].itemTime - frameTime;

			//Once the 10 seconds have passed, and an item has passed its expiration date it gets deleted in the same way as if it were collected
			if (items[i].itemTime <= sf::seconds(0.0f))
			{
				items.erase(items.begin() + i); 
			}
		}

		//Spawning
		if (!gameover) 
		{
			itemspawnremaining = itemspawnremaining - frameTime;
			if (itemspawnremaining <= sf::seconds(0.0f))
			{
				items.push_back(Item(itemTextures, gameWindow.getSize()));
				itemspawnremaining = itemspawnduration;
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			// when escape is pressed manually exit the program
			gameWindow.close();
		}

		//collision

		//In the original Template you placed this inside the loop for checking collision, however this is unneccessary as it all occurs within a single frame and so the player doesnt move
		// per each time you check. Not an issue with a game this simple, but its probably bad practice
		sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();
		sf::FloatRect playerBounds2 = playerObject2.sprite.getGlobalBounds();


		for (int i = items.size() - 1; i >=0; --i) 
		{
			sf::FloatRect itemBounds = items[i].sprite.getGlobalBounds();

			if (itemBounds.intersects(playerBounds)) //if the player has touched an item
			{
				playerObject.score += items[i].pointsvalue; //add the points value of the coin we picked up to the total player score
				items.erase(items.begin() + i); //the item is removed from the list, thus removing its sprite. Basically, delete the item once picked up

				//play the pickup sound effect
				pickUpSound.play();
			}

			if (itemBounds.intersects(playerBounds2)) //if the player has touched an item
			{
				playerObject2.score += items[i].pointsvalue; //add the points value of the coin we picked up to the total player score
				items.erase(items.begin() + i); //the item is removed from the list, thus removing its sprite. Basically, delete the item once picked up

				//play the pickup sound effect
				pickUpSound.play();
			}
		}

		//Game Ending
		if (timeRemaining.asSeconds() <= 0) 
		{
			//set the time remaining to 0
			timeRemaining = sf::seconds(0);
			
			//check if this is the first time gameover is true, as to stop it looping
			if (gameover == false) 
			{
				//Stop game music
				gameMusic.stop();
				//Play victory music
				victorySound.play();
				//set gameover to true
				gameover = true;
			}

		}

		//game restart
		if (gameover) 
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R)) 
			{
				//restart the game
				gameover = false;
				timeRemaining = timeLimit;
				gameMusic.play(); //as the initial playing of music is done within the setup, we have to put this in here

				//spawn 3 intial items and clear the screen of items from the last game
				items.clear();
				items.push_back(Item(itemTextures, gameWindow.getSize()));
				items.push_back(Item(itemTextures, gameWindow.getSize()));
				items.push_back(Item(itemTextures, gameWindow.getSize()));

				//restart the player back to the middle
				playerObject.reset(gameWindow.getSize());
				playerObject2.reset(gameWindow.getSize());

				//reset the score
				playerObject.score = 0;
				playerObject2.score = 0;
			}
		}

		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------

		gameWindow.clear(sf::Color::Black);
		gameWindow.draw(titleText);
		gameWindow.draw(authorText);
		gameWindow.draw(playerObject.scoreText);
		gameWindow.draw(playerObject2.scoreText);
		gameWindow.draw(timerText);
		
		if (!gameover) //only draw these when its not game over
		{
			gameWindow.draw(playerObject.sprite);
			gameWindow.draw(playerObject2.sprite);

			for (int i = 0; i < items.size(); ++i) //loop to draw each coin in items
			{
				gameWindow.draw(items[i].sprite);
			}
		}

		if (gameover) 
		{
			if (playerObject.score >= playerObject2.score) 
			{ 
				gameWindow.draw(playerObject.gameOverText); 
			}

			else
			{
				gameWindow.draw(playerObject2.gameOverText);
			}
			
			gameWindow.draw(restartText);
		}


		gameWindow.display();
	}
	// End of Game Loop
	return 0;
}
// End of main() Function