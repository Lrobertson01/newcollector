#include "Player.h"
#include <SFML/Graphics.hpp> //neccessary to move the scoretext into this file

Player::Player(sf::Texture& playertexture, sf::Vector2u screensize) 
{
	sprite.setTexture(playertexture);

	sprite.setPosition
	(screensize.x / 2 - playertexture.getSize().x / 2, screensize.y / 2 - playertexture.getSize().y / 2);

	velocity.x = 0.0f;
	velocity.y = 0.0f;

	speed = 300.0f;




	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::White);

	gameOverText.setString("GAME OVER");
	gameOverText.setCharacterSize(24);
	gameOverText.setFillColor(sf::Color::Cyan);
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	
	

}

void Player::Input()
{

	//Velocity set to 0 each update, to then be changed prior to the movement update
	velocity.x = 0.0f;
	velocity.y = 0.0f;

	if (playerIndex == 0) 
	{

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			// right key is pressed: move our character right
			velocity.x = speed;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			// left key is pressed: move our character left
			velocity.x = -speed;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			// down key is pressed: move our character down
			velocity.y = speed;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			// up key is pressed: move our character up
			velocity.y = -speed;
		}
	}

	if (playerIndex == 1)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			// right key is pressed: move our character right
			velocity.x = speed;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			// left key is pressed: move our character left
			velocity.x = -speed;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			// down key is pressed: move our character down
			velocity.y = speed;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			// up key is pressed: move our character up
			velocity.y = -speed;
		}
	}

}

void Player::Update(sf::Time frameTime)
{
	//set the character to move when they have a velocity
	sprite.setPosition(sprite.getPosition() + velocity * frameTime.asSeconds());
	
	//have to set position here, as if i set with the rest of the scoretext it wont get the index and both messages spawn inside each other
	scoreText.setPosition(30, (playerIndex * 30) + 30);

}

void Player::reset(sf::Vector2u screensize)
{
	sprite.setPosition
	(screensize.x / 2 - sprite.getTexture() ->getSize().x / 2, screensize.y / 2 - sprite.getTexture()->getSize().x / 2);
}


